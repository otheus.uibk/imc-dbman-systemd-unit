# iMC-dbman systemd unit

A systemd-unit service file for HP's iMC's dbman utility.

## Instllation

1. copy contents into /etc/systemd/system/dbman.conf
2. Adjust paths as needed, for instance, if anything 
is not installed in `/opt/iMC/dbman`
3. reload systemd:

    `systemctl daemon-reload dbman`

4. kill the old one that is started from rc.local

    `pkill dbman`

5. start the new one:

    `systemctl start dbman`

6. Check for errors/problems:

    `systemctl status dbman`

7. If everything looks good, continue
8. Enable the service

    `systemctl enable dbman`

9. Remove the old startup from `/etc/rc.local`

